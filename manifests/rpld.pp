class netboot::rpld {

## boot novell - netboot bios
#  TODO: configure /etc/default/rpld
#        configure /etc/rpld.conf for pxe booting
#        http://help.lockergnome.com/linux/Netbooting-RPL--ftopict415270.html
#       http://etherboot.org/wiki/rplchaining
#
  package { [ 'rpld' ]: ensure => installed }

  service { 'rpld':
    ensure    => running,
    enable    => true,
    subscribe => [ Package['rpld'], File['/etc/rpld.conf'], File['/etc/default/rpld'] ],
  }


  # rpld.conf configuration.
  file { '/etc/rpld.conf':
    owner  => root,
    group  => root,
    mode   => '0644',
    source => 'puppet:///modules/netboot/rpld.conf'
  }

  file { '/etc/default/rpld':
    owner  => root,
    group  => root,
    mode   => '0644',
    source => 'puppet:///modules/netboot/rpld'
  }

  # download the
  #  http://rom-o-matic.net/gpxe/gpxe-git/gpxe.git/contrib/rom-o-matic/
  # gPXE 0.9.9+
  # pxe bootstraploader / all driver
  file { '/var/lib/tftpboot/gpxe.pxe':
    owner  => root,
    group  => root,
    mode   => '0644',
    source => 'puppet:///modules/netboot/gpxe.pxe'
  }


  class tftpd {
    package { 'tftpd-hpa': ensure => installed }

    file_line { 'tftpd_enable':
      path  => '/etc/default/tftpd-hpa',
      match => '^RUN_DAEMON=\"(?!yes).*\"',
      line  => 'RUN_DAEMON=yes'
    }
  }
}

