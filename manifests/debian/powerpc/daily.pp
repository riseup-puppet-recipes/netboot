class netboot::debian::powerpc::daily {

  package { [ 'tftpd-hpa' ]: ensure => installed }

  file { ['/var/lib/tftpboot/debian-installer',
          '/var/lib/tftpboot/debian-installer/daily',
          '/var/lib/tftpboot/debian-installer/daily/powerpc/']:
    ensure  => directory,
    owner   => root,
    group   => root,
    require => Package['tftpd-hpa'];
  }

  fetch_netboot_powerpc { [ 'boot.msg', 'initrd.gz', 'vmlinux',
                            'yaboot', 'yaboot.conf' ]:  }

  $mirror_daily_di = 'http://d-i.debian.org/daily-images/powerpc/daily/powerpc/netboot'

  define fetch_netboot_powerpc($mirror = 'http://d-i.debian.org/daily-images/powerpc/daily/powerpc/netboot') {

    #   file { ['/var/lib/tftpboot/debian-installer/daily1', '/var/lib/tftpboot/debian-installer/daily1/powerpc/']:
    #      ensure  => directory,
    #      owner   => root,
    #      group   => root,
    #      require => [Package['tftpd-hpa']];
    #    }

    # FIXME: is the mirror1 below a bug?
    exec { "wget -O /var/lib/tftpboot/debian-installer/daily/powerpc/${name} -q ${mirror1}/${name}":
      creates => "/var/lib/tftpboot/debian-installer/daily/powerpc/${name}",
      path    => '/usr/bin',
      require => [Package['tftpd-hpa'], File['/var/lib/tftpboot/debian-installer/daily/powerpc/']]
    }
  }
}
