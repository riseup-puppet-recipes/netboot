class netboot::debian::powerpc::squeeze {

  file { ['/var/lib/tftpboot/debian-installer/squeeze',
          '/var/lib/tftpboot/debian-installer/squeeze/powerpc/']:
    ensure  => directory,
    owner   => root,
    group   => root,
    require => Package['tftpd-hpa'];
  }

  fetch_netboot_powerpc_squeeze { [ 'boot.msg', 'initrd.gz', 'vmlinux',
                                    'yaboot', 'yaboot.conf' ]:
                                    release => 'squeeze';  }

  #   $mirror1 = 'http://debian.torredehanoi.org/debian/dists/squeeze/main/installer-powerpc/current/images/powerpc/netboot'

  define fetch_netboot_powerpc_squeeze($release,$mirror='http://debian.torredehanoi.org/debian/dists') {

    exec { "wget -O /var/lib/tftpboot/debian-installer/${release}/powerpc/${name} -q ${mirror}/${release}/main/installer-powerpc/current/images/powerpc/netboot/${name}":
      creates => "/var/lib/tftpboot/debian-installer/${release}/powerpc/${name}",
      path    => '/usr/bin',
      require => [Package['tftpd-hpa'], File["/var/lib/tftpboot/debian-installer/${release}/powerpc/"]]
    }
  }
}
