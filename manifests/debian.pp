class netboot::debian {

  package { 'di-netboot-assistant': ensure => installed }

  # Current di-netboot-assistant has an up to date di-sources.list
  # this code commented in case we need to ship our own again
  #file { '/etc/di-netboot-assistant/di-sources.list':
  #  source  => [ 'puppet:///modules/site_netboot/tftp/di-sources.list', 'puppet:///modules/netboot/di-sources.list' ],
  #  mode    => '0644',
  #  owner   => root,
  #  group   => root,
  #  notify  => Exec['di-netboot-assistant rebuild-menu'],
  #  require => Package['di-netboot-assistant']
  #}

  file { '/etc/di-netboot-assistant/pxelinux.HEAD':
    source  => [ 'puppet:///modules/site_netboot/tftp/pxelinux.HEAD', 'puppet:///modules/netboot/pxelinux.HEAD' ],
    mode    => '0644',
    owner   => root,
    group   => root,
    notify  => Exec['di-netboot-assistant rebuild-menu'],
    require => Package['di-netboot-assistant']
  }

  exec { 'di-netboot-assistant rebuild-menu':
    path        => '/bin:/sbin:/usr/sbin:/usr/bin',
    refreshonly => true
  }

  # symbolic link to generated menu
  file  {
    '/var/lib/tftpboot':
      ensure => directory;

    # create symlinks in the tftproot pointing to the d-i provided pxe files
    '/var/lib/tftpboot/pxelinux.0':
      ensure => 'd-i/n-a/pxelinux.0';

    '/var/lib/tftpboot/ldlinux.c32':
      ensure => 'd-i/n-a/ldlinux.c32';

    '/var/lib/tftpboot/libcom32.c32':
      ensure => 'd-i/n-a/libcom32.c32';

    '/var/lib/tftpboot/libutil.c32':
      ensure => 'd-i/n-a/libutil.c32';

    '/var/lib/tftpboot/pxelinux.cfg':
      ensure => directory,
      owner  => root,
      group  => root,
      mode   => '0755';

    # default to using dna's default
    '/var/lib/tftpboot/pxelinux.cfg/default':
      ensure => '../d-i/n-a/pxelinux.cfg/default';
  }

  define di_menu ( $label='', $distro='jessie', $arch='amd64', $append='',
                   $url="http://${hostname}/d-i/${distro}/./preseed.cfg",
                   $checksum='', $get_hostname='',
                   $kernel="debian-installer/${distro}/${arch}/linux" )
  {

    $base_append = "locale=en_US console-keymaps-at/keymap=us netcfg/get_hostname=${get_hostname} netcfg/get_domain=${::domain} url=${url} preseed/file/checksum=${checksum} initrd=debian-installer/${distro}/${arch}/initrd.gz"
    $real_append = "${append} ${base_append}"

    file { "/var/lib/di-netboot-assistant/${name}.pxelinux.menu.fragment":
      ensure  => 'present',
      content => template('netboot/pxe.fragment.cfg.erb'),
      notify  => Exec['di-netboot-assistant rebuild-menu']
    }

    exec { $name:
      command => "di-netboot-assistant install ${distro} --arch=${arch}",
      creates => "/var/lib/tftpboot/debian-installer/${distro}/${arch}/linux",
      path    => '/bin:/sbin:/usr/sbin:/usr/bin',
      require => [ Package['di-netboot-assistant'],
                   File['/etc/di-netboot-assistant/pxelinux.HEAD'] ]
    }
  }
}
